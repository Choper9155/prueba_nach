package com.example.prueba.nach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaNachApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaNachApplication.class, args);
	}

}
